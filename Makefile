packer/output-qemu/omadacp.qcow2: packer/ephemeral/omadacp.tar packer/ephemeral/installercore.iso
	-rm packer/output-qemu/omadacp.qcow2
	cd packer && make output-qemu/omadacp.qcow2

packer/output-qemu/omadacp.qcow2.xz: packer/output-qemu/omadacp.qcow2
	xz -T0 packer/output-qemu/omadacp.qcow2

persistent.qcow2:
	qemu-img create -f qcow2 persistent.qcow2 1G

scratch-interactive: packer/output-qemu/omadacp.qcow2 metadata/mddata.iso persistent.qcow2
	qemu-system-x86_64 -accel kvm -nographic -m 512 -smp 2 -drive file=packer/output-qemu/omadacp.qcow2,if=virtio \
		-cdrom metadata/mddata.iso \
		-drive file=persistent.qcow2,if=virtio \
		-device virtio-net-pci,netdev=user -netdev user,id=user \
		-device virtio-net-pci,netdev=n1,mac=52:54:00:4f:55:75 -netdev socket,id=n1,mcast=230.0.0.1:2001

packer/ephemeral/omadacp.tar: build.sh Makefile docker/Dockerfile
	CODEBASE=omadacp ./build.sh
	-docker rm export
	docker run --name export build/release true
	docker export export > packer/ephemeral/omadacp.tar

packer/ephemeral/installercore.iso:
	docker run --rm=true -v $$(pwd)/packer/ephemeral:/workdir:Z registry.gitlab.com/untrustedhost/installenv
	sudo chown $$(whoami) packer/ephemeral/installercore.iso

metadata/mddata.iso: metadata/Makefile metadata/makeiso.sh metadata/mangle-example.sh metadata/virt-install.xml
	cd metadata && make mddata.iso
