#!/usr/bin/env bash

# broken out into separate piece for reusing state logic
mountpoint -q /state || exit 1

[[ -d /state/omadacp ]] || mkdir /state/omadacp

for ent in logs data work ; do
[[ -d "/state/omadacp/${ent}" ]] || mkdir "/state/omadacp/${ent}"
mountpoint -q "/var/opt/tplink/EAPController/${ent}" || mount -o bind "/state/omadacp/${ent}" "/var/opt/tplink/EAPController/${ent}"
case "${ent}" in data) mkdir -p "/state/omadacp/${ent}/db" ;; esac
chown -R omada:omada "/var/opt/tplink/EAPController/${ent}"
done

:
