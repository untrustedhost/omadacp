#!/usr/bin/env bash

unlock(){ : ; }

failboat() {
  echo "${@}" 1>&2
  unlock
  exit 1
}

my_service=tpeap

# server ip address from here
V4XML="/run/untrustedhost/netxml/${my_service}.xml"
RUNTIME="/run/anycast-healthchecker/${my_service}.check-conf"
LOCK="/run/anycast-healthchecker/${my_service}.lock"

[[ -e "${V4XML}" ]] || failboat "no interface xml"

lock_retry=0

lock(){
        lockfile-create -r $lock_retry -p "${LOCK}" && return 0
        echo "ERROR: Can't get lock"
        exit $?
}

unlock(){ lockfile-remove "${LOCK}" ; }

getaddrsv4() {
  local prefix
  prefix=$1
  local co="/run/untrustedhost/netxml/${prefix}.xml"
  [[ -f "${co}" ]] || { err "missing ${co}" ; return 1 ; }
  local v4candidates=($(xmlstarlet sel -t -v 'address/@ipv4' "${co}"))
  local addr ; res=()
  for addr in "${v4candidates[@]}" ; do
    local net=''
    net="$(ipcalc "${addr}"|awk '$1 == "Network:" { print $2 }')"
    case "${net}" in
      */31) : ;;
      *)    err "non/31 ipv4 range supplied" ; exit 1 ;;
    esac
    [[ "${net}" == "${addr}" ]] && res=("${res[@]}" "${net}")
  done
  printf '%s\n' "${res[@]}"
}

update_rtconf() {
	local addrs=()
	addrs=($(getaddrsv4 "${my_service}"))
	hx="$(ipcalc "${addrs[0]}"|awk '$1 == "HostMax:" { print $2 }')"
	printf 'SIADDR=%s\n' "${hx}" > "${RUNTIME}"
}

lock
[[ -e "${RUNTIME}" ]] || update_rtconf
[[ "${RUNTIME}" -nt "${V4XML}" ]] || update_rtconf

. "${RUNTIME}"

[[ "${SIADDR}" ]] || failboat "no server address in v4conf?!"

# do I have a task?
systemctl status tpeap > /dev/null 2>&1 || failboat "tpeap not running (yet?)"

# curl and see if we get a response
curl http://${SIADDR}:8088/api/info | jq . > /dev/null 2>&1 || failboat "api info query failed"

# look by this point everything worked, soooo :)
