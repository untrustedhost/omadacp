#!/usr/bin/env bash

source="virt-install.xml"

# /31 for the anycast-land
# NOTE: bird will validate this starts correctly ;)
omadacp_net='172.16.15.10/31'

# bird should _really_ have a per-router id. we dhcpd' though, so pick something.
routerid="240.128.0.0"
asn="65535"

# take an ifindex and associate to an octel-delim OSPF zone
bridgename[0]='green'
bridgename[1]='red'
bridgename[2]='blue'
ospf_area[0]='0.0.0.0'

# set an id/key for an OSPF zone (in hex)
ospf_key_00000000[4]='MeitIf*ogaximut6'

# shouldn't need to change these - paths for interface bridgenames
xp_bridge='/domain/devices/interface'
xp_bridgenames="${xp_bridge}/source/@bridge"

# array to hold all the edit args to xmlstarlet
xmlstarlet_args=()

# start by filtering out what _type_ of vm this is. ;)
node_filter=('domain/@type')

# filter out all manner of things based on el output now
while read -r nodeline ; do
  has_parent=0
  case "${nodeline}" in
    # keep the below nodes
    domain|domain/name|domain/devices|domain/devices/interface*) : ;;
    *) 
      # hm. is a parent in node_filter yet?
      potential="/${nodeline}"
      while [ "${potential}" != "" ] ; do
        # strip an element...
        potential="${potential%/*}"
        # explicit break if that _was_ the last element
        [[ -z "${potential}" ]] && break

        # compare to space-delim string of current entities
        case " ${node_filter[*]} " in
          *" ${potential#/} "*) has_parent=1 ;;
        esac
      done

      # if we didn't find a parent, add this now.
      [[ "${has_parent}" -eq 1 ]] || node_filter=("${node_filter[@]}" "${nodeline}")
    ;;
  esac
done < <(xmlstarlet el "${source}")

# turn _that_ into delete calls for xmlstarlet
for filt in "${node_filter[@]}" ; do
  xmlstarlet_args=("${xmlstarlet_args[@]}" "-d" "${filt}")
done

# walk the interfaces and add IPs, MTUs via xmlstarlet calls...
for bridge in $(xmlstarlet sel -t -v "${xp_bridgenames}" < "${source}") ; do
  # create ipv4 subnode and set address elem in it.
  ctr=0
  for label in "${bridgename[@]}" ; do
    [[ "${bridge}" == "${label}" ]] && {
      # non-zero ip...
      [[ "${ipv4[${ctr}]}" ]] && {
        xmlstarlet_args=("${xmlstarlet_args[@]}" 
          -s "${xp_bridge}[source/@bridge=\"${bridge}\"]" -t 'elem' -n 'ipv4' -v ''
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/ipv4" -t attr -n 'address' -v "${ipv4[${ctr}]}")
      }
      # mtu?
      [[ "${mtu[${ctr}]}" ]] && {
        xmlstarlet_args=("${xmlstarlet_args[@]}" 
          -s "${xp_bridge}[source/@bridge=\"${bridge}\"]" -t 'elem' -n 'mtu' -v ''
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/mtu" -t attr -n 'size' -v "${mtu[${ctr}]}")
      }
      # ospf area?
      [[ "${ospf_area[${ctr}]}" ]] && {
        xmlstarlet_args=("${xmlstarlet_args[@]}"
          -s "${xp_bridge}[source/@bridge=\"${bridge}\"]" -t 'elem' -n 'ospf' -v ''
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/ospf" -t attr -n 'area' -v "${ospf_area[${ctr}]}")
      }
    }
    ctr=$((ctr + 1))
  done
done

# move domain node inside metadata now
xmlstarlet_args=("${xmlstarlet_args[@]}"
                    -s / -t 'elem' -n "metadata" -v ''
                    -m //domain //metadata)

# omadacp configuration
xmlstarlet_args=("${xmlstarlet_args[@]}"
    -s /metadata -t 'elem' -n "omadacp" -v ''
    -s /metadata/omadacp -t 'elem' -n "address" -v ''
    -i /metadata/omadacp/address -t attr -n 'ipv4' -v "${omadacp_net}")

# router configuration
xmlstarlet_args=("${xmlstarlet_args[@]}"
    -s /metadata -t 'elem' -n 'router' -v ''
    -i /metadata/router -t attr -n 'id' -v "${routerid}")

# ospf zone authentication configuration
ospf_seen=()
for area in "${ospf_area[@]}" ; do
  case " ${ospf_seen[*]} " in
    *" ${area} "*) continue ;;
  esac

  # convert area from octet to hex for lookup...
  area_hx=$(printf '%02X' ${area//./ })
  handle="ospf_key_${area_hx}"
  # this is...the only way to get the keys of an array dynamically AFAICT.
  id_keys=$(eval echo \${!${handle}[*]})

  # check the results, then go get passphrases
  [[ "${id_keys}" ]] && {
    xmlstarlet_args=("${xmlstarlet_args[@]}" -s /metadata/router -t 'elem' -n 'ospf' -v '' -i /metadata/router/ospf -t attr -n 'area' -v "${area}")
    for keyid in ${id_keys} ; do
      pass=$(eval echo \${${handle}[${keyid}]})
      xmlstarlet_args=("${xmlstarlet_args[@]}"
                         -s "/metadata/router/ospf[@area=\"${area}\"]" -t 'elem' -n 'authentication' -v ''
                         -s "/metadata/router/ospf[@area=\"${area}\"]/authentication[last()]" -t 'attr' -n 'key' -v "${keyid}"
                         -s "/metadata/router/ospf[@area=\"${area}\"]/authentication[@key=\"${keyid}\"]" -t 'attr' -n 'password' -v "${pass}")
    done
  }
  ospf_seen=("${ospf_seen[@]}" "${area}")
done


# finally, run xmlstarlet and enjoy.
xmlstarlet ed "${xmlstarlet_args[@]}" "${source}"
